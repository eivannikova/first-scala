package com.scala.first

import org.apache.spark.SparkContext

object Main {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "MyApp")
    val file = sc.textFile("example.txt")
    val res = file.filter(_.contains("implements"))
    val cache = res.cache()
    val res2 = cache.collect()
    println(res2.deep.mkString)
  }
}